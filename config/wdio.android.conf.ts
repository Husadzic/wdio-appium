import  { config }  from "./wdio.conf"
import * as path from 'path'


config.capabilities = [{
    'appium:platformName': 'Android',
    'appium:platformVersion': '12.0',
    'appium:deviceName': 'Pixel 6 Pro',
    'appium:automationName': 'UIAutomator2',
    'appium:app': 'https://jasminhusadzic.github.io/wdio-dummy-app/ApiDemos-debug.apk'
    // 'appium:app': path.join(process.cwd(), 'app/android/ApiDemos-debug.apk')
    }
]

exports.config = config;
